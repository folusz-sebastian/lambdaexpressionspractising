package lambdaExpressionsPractising;

import java.util.Arrays;
import java.util.List;

public class inventoryMain {
    public static void main(String[] args) {
        List<Apple> inventory = Arrays.asList(
                new Apple(80, "green"),
                new Apple(155, "green"),
                new Apple(120, "red")
        );
        AppleService.filter(inventory, (Apple apple) -> "green".equals(apple.getColor()));
    }
}
