package lambdaExpressionsPractising;

import java.util.ArrayList;
import java.util.List;

public class AppleService {
    public static <T> List<T> filter(List<T> inventory, Predicate<T> p) {
        List<T> result = new ArrayList<>();
        for (T element : inventory) {
            if (p.test(element))
                result.add(element);
        }
        return result;
    }

    public interface Predicate<T> {
        boolean test(T t);
    }
}
